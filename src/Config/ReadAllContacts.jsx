import axios from "axios";
import React, { useEffect, useState } from "react";
// import { baseUrl } from "../../Config/Config";

const ReadAllContacts = () => {
  let [contacts, setContact] = useState([]);
  let readAllContact = async () => {
    let info = {
      url: `https://fake-api-nkzv.onrender.com/api/v1/contacts`,
      method: "get",
    };

    let result = await axios(info);

    setContact(result.data.data.results);
  };

  useEffect(() => {
    readAllContact();
  }, []);
  let deleteContact = async (_id) => {
    let obj={url: `https://fake-api-nkzv.onrender.com/api/v1/contacts/${_id}`,method: "delete"}
    let result = axios(obj);

  }
  
  // localStorage.setItem('a',"nitan")
  // localStorage.getItem("a")
  

  return (
    <div>
      {contacts.map((item, i) => {
        return (
          <div key={i} style={{ border: "solid red 3px" }}>
            <p> Full name : {item.fullName}</p>
            <p> Email : {item.email}</p>
            <p> Address : {item.address}</p>
            <p> Phone Number : {item.phoneNumber}</p>
            <button onClick={async ()=>{
                await deleteContact (item._id)
                await readAllContact()
            }}>Delete</button>
           
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllContacts;










