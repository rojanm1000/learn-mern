import React from 'react'
import { useState } from 'react'

export default function HideAndShow() {
    let [ show,setShow] = useState(true)
  return (
    <div>

        {show ? <p>Error has occured</p>
        :null}
        <button
        onClick = {()=>{
            setShow(true);
        }
        } 
        >show</button>
        <button
        onClick = {()=>{
            setShow(false);
        }}
        >hide</button>

      
    </div>
  )
}
