import React, { useState } from 'react'

const Counter = () => {
    let [count,setCount] = useState(0)

  return (
    <div>
        count= {count}
        <br></br>
        <button
            onClick = {() => {
                setCount(count+1)
            }}
        >increment</button>

<button
            onClick = {() => {
                setCount(count-1)
            }}
        >decrement</button>

<button
            onClick = {() => {
                setCount(0)
            }}
        >reset</button>
      
    </div>
  )
}

export default Counter
