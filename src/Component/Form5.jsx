import React, { useState } from 'react'
const Form5 = () => {
    let[productName, setProductName] = useState('')
    let[proManEmail, setProManEmail] = useState('')
    let[staffEmail, setStaffEmail] = useState('')
    let[password, setPassword] = useState('')
    let[gender, setGender] = useState('')
    let[proManuDate, setProManuDate] = useState('')
    let[MangerIsMarried, setManagerIsMarried] = useState('')
    let[ManagerSpouse, setManagerSpouse] = useState('')
    let[proLocation, setProLocation] = useState('')
    let[proDescription, setProDescription] = useState('')
    let[productIsAvailable, setProAvailable] = useState('')
    let Genders = [{ label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" }]
  return (
    <form onSubmit={(e)=>{
        e.preventDefault()
        let info ={productName, proManEmail ,staffEmail, gender, proManuDate, proLocation, proDescription, productIsAvailable}
    console.log(info)}}>
        <div className='bg-red'><p><b>Products Information Form</b></p></div><br></br>
        <label htmlFor='productName'>Product's Name: </label>
        <input id='productName' type = 'text' placeholder="Enter product's name" value ={productName}
        onChange={(e)=>{
            setProductName(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='proManEmail'>Product Manager's E-mail: </label>
        <input id='proManEmail' type = 'email' placeholder="Enter product manager e-mail" value ={proManEmail}
        onChange={(e)=>{
            setProManEmail(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='staffEmail'>Staff's E-mail: </label>
        <input id='staffEmail' type = 'email' placeholder="Enter staff's E-mail" value ={staffEmail}
        onChange={(e)=>{
            setStaffEmail(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='password'>Password: </label>
        <input id='password' type = 'password' placeholder="Enter password" value ={password}
        onChange={(e)=>{
            setPassword(e.target.value)
        }}>
        </input>
        <br></br>
        <label>Gender: </label>
        {Genders.map((item,i)=>{
          return <>
          <label key = {i} htmlFor= {item.value}>{item.label}</label>
          <input checked={gender === item.value}
          onChange={(e)=>{setGender(e.target.value)}}
          type = 'radio' id={item.value} value={item.value}>
          </input>
          </>
        })}
        <br></br>
        <label htmlFor='proManuDate'>Product Manufacture Date: </label>
        <input id='proManuDate' type = 'date' value ={proManuDate}
        onChange={(e)=>{
            setProManuDate(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='MangerIsMarried'> Is Manager Married? </label>
        <input id='MangerIsMarried' type = 'checkbox' value ={MangerIsMarried}
        onChange={(e)=>{
            setManagerIsMarried(e.target.checked)
        }}>
        </input>
        <br></br>
        <label htmlFor='ManagerSpouse'>Manager's Spouse: </label>
        <input id='ManagerSpouse' type = 'text' placeholder="Enter manager's spouse" value ={ManagerSpouse}
        onChange={(e)=>{
            setManagerSpouse(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='proLocation'>Product's Location: </label>
        <input id='proLocation' type = 'text' placeholder="Enter product's location" value ={proLocation}
        onChange={(e)=>{
            setProLocation(e.target.value)
        }}>
        </input>
        <br></br>
        <label htmlFor='proDescription'>Product's Description: </label>
        <textarea id='proDescription' type = 'text' placeholder="Enter product's description" value ={proDescription}
        onChange={(e)=>{
            setProDescription(e.target.value)
        }} rows = {5} cols ={50}>
        </textarea>
        <br></br>
        <label htmlFor='productIsAvailable'>Is Product Available? </label>
        <input id='productIsAvailable' type = 'checkbox' value ={productIsAvailable}
        onChange={(e)=>{
            setProAvailable(e.target.checked)
        }}>
        </input>
        <br></br>
        <button type='submit'>Send </button>
    </form>
  )
}
export default Form5