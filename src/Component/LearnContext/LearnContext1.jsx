import React, { useContext } from 'react'
import { CountContext, infoContext } from '../../App'
import LearnUseContext2 from './LearnUseContext2'

const LearnContext1 = () => {
    let data = useContext(infoContext)
    let c1 = useContext(CountContext)
  return (
    <div>
        {data}<br></br>
        count = {c1.count}
        <LearnUseContext2 ></LearnUseContext2>
        <button onClick={()=>{
            c1.setCount(c1.count+1)
        }}>Click here</button>
      
    </div>
  )
}

export default LearnContext1
