import React from 'react'
import {Route, Routes} from "react-router-dom";
import About from './About';
import Home from './Home';
import LearnToGetDynamicRoute from './LearnToGetDynamicRoute';
import LearnToSearchParams from './LearnToSearchParams';

const Routesss = () => {
  return (
    <div>
          <Routes>
                    <Route path = "/Home" element = {<Home></Home>}></Route>
                    <Route path = "/About" element = {<About></About>}></Route>
            <Route path ="/Contact" element = {<div>Contact</div>}></Route>
            <Route path ="/Contact/1" element = {<div>Contact</div>}></Route>
            <Route
            path="/Contact/:id1/id/:id2"
            element = {<LearnToGetDynamicRoute></LearnToGetDynamicRoute>}
            ></Route>
            {/* <Route path='/Product' element = {<LearnToSearchParams></LearnToSearchParams>}></Route> */}

        </Routes>
        

        
      
      
    </div>
  )
}

export default Routesss
