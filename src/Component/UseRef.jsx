import React, { useRef } from 'react'

const UseRef = () => {
    let inputRef1 = useRef()
    let inputRef2 = useRef()
  return (
    <div>
      <p ref = {inputRef1}> Hello my name Rojan</p>
      <p ref = {inputRef2}>Hello im Nothing</p>
      <br></br>
      <button onClick={()=>{
        inputRef1.current.style.backgroundColor ="Orange"
        inputRef2.current.style.backgroundColor = "Purple"




      }}>Change Color</button>
    </div>
  )
}

export default UseRef
