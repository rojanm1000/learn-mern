import React, { useState } from 'react'

const Form4 = () => {
    let [Name, setName] = useState("")
    let [Address, setAddress] = useState("")
    let [Password, setPassword] = useState("")
    let [Email, setEmail] = useState("")
    let [Number, setNumber] = useState("")
    let [Gender, setGender] = useState("")
    let [Age, setAge] = useState("")
    let [Comment, setComment] = useState("")
    let [IsMarried, setIsMarried] = useState("")
    let [role, setRole] = useState("")


return (
    <form onSubmit = {(e) => {
        e.preventDefault()
        let info = {
            name:Name,
            address:Address,
            password:Password,
            email:Email,
            Number:Number,
            Age:Age,
            Comment:Comment,
            IsMarried:IsMarried,
            Gender,
            role:role,
        }
        console.log(info)}}
    >
        <label htmlFor='Name'>Name:</label>
        <input type = "text" placeholder = "type your name" value = {Name} onChange = {(e)=>{setName(e.target.value)}}></input>
        <br></br>
        <label htmlFor='Address'>Address:</label>
        <input value = {Address} placeholder = "Address" onChange = {(e)=>{setAddress(e.target.value)}}></input>
        <br></br>
        <label htmlFor='Password'>Password:</label>
        <input type = "password" placeholder='Password' value = {Password} onChange = {(e)=>{setPassword(e.target.value)}}></input>
        <br></br>
        <label htmlFor='email'>Email:</label>
        <input type = "email" placeholder="type your email" value = {Email} onChange = {(e)=>{setEmail(e.target.value)}}></input>
        <br></br>
        <label htmlFor='Number'>Number</label>
        <input type = "Number" placeholder='Number' value = {Number} onChange = {(e)=>{setNumber(e.target.value)}}></input>
        <br></br>
        <label htmlFor='Age'>Age:</label>
        <input type = "Number" placeholder='Age' value = {Age} onChange = {(e)=>{setAge(e.target.value)}}></input>
        <br></br>
        <label>Married</label>
        <input type = "checkbox" value = {IsMarried} onChange = {(e)=>{setIsMarried(e.target.checked)}}></input>
        <br></br>
        <label for = 'Comment'>Comment:</label>
        <textarea id="Comment" name="Comment" rows="4" cols="10"></textarea>
        <br></br>





<label >Gender: </label>
        <label htmlFor = "male">male</label>
        <input 
        checked = {Gender ==="male"}
        onChange = {(e)=>{setGender(e.target.value)}}
        
        
        type = "radio" id = "male" value = "male"></input>
        <label htmlFor = "female">female</label>
        <input
        checked = {Gender ==="female"}
        onChange = {(e)=>{setGender(e.target.value)}}

        
        type = "radio" id = "female" value = "female"></input>
        <label htmlFor = "others">others</label>

        <input 
        checked = {Gender ==="others"}
        onChange = {(e)=>{setGender(e.target.value)}}

        type = "radio" id = "others" value = "others"></input>
        <br></br>
        <button type = "submit">Send</button>
       










      
    </form>
  )
}

export default Form4
