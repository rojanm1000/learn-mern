import React from 'react'

const Form1 = () => {
  return (
   <>
  <form onSubmit ={(e)=>{
    e.preventDefault();
    console.log("form is submitted")
  }}>
    <input placeholder = "name"></input>
    <br></br>
    <input placeholder = "password" type = "password"></input>
    <br></br>
    <input type = "number"></input>
    <br></br>
    <input type = "email" placeholder = "email"></input>
    <br></br>
    <input type = "file"></input>
    <br></br>
    <input type = "date"></input>
    <br></br>
    <button type = "submit">Send</button>
    <button type = "button">Click</button>
  </form>
  </>
    
  )
}

export default Form1
