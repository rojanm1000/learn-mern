import React, { useState } from 'react'

const Form2 = () => {
    let [Name, setName] = useState("")
    let [Address, setAddress] = useState("")
    let [Password, setPassword] = useState("")
    let [country, setCountry] = useState("")  
    let [movies, setMovies] = useState("")
    let [gender, setGender] = useState("")



let countries = [
      { label: "Select Country", value: "", disabled: true },
      { label: "Nepal", value: "nepal" },
      { label: "China", value: "china" },
      { label: "India", value: "india" },
      { label: "America", value: "america" },
];

  let favMovies = [
  { label: "Select Movies", value: "", disabled: true },
  { label: "Fast And Furious", value: "fast and furious" },
  { label: "Avenger", value: "avenger" },
  { label: "Inception", value: "inception" },
  { label: "FightClub", value: "fightClub" },
  ];

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <form onSubmit = {(e) => {
            e.preventDefault()
            let info = {
                name:Name,
                address:Address,
                password:Password,
                country:country,
                movies:movies,
                gender:gender,
                
                
            }
            console.log(info)
        }}>
        <label htmlFor = "a">Name:</label>
        <input id = "a" type = "text" placeholder = "type your name" value = {Name} onChange = {(e)=>{setName(e.target.value)}}></input>
        <br></br>
        <input value = {Address} onChange = {(e)=>{setAddress(e.target.value)}}></input>
        <br></br>
        <input type = "password" value = {Password} onChange = {(e)=>{setPassword(e.target.value)}}></input>
        <br></br>
        <button type = "submit">Send</button>
        <br></br>
        <label htmlFor = "country">Country:</label>
      
        <select 
        id = "country"
        value = {country}
        onChange = {(e)=>{setCountry(e.target.value)}}
        
        >
          {/* <option disabled = {true}>Select Country</option>
          <option value = "nep">Nepal</option>
          <option value = "ind">India</option>
          <option value = "US">USA</option>
          <option value = "UK">UK</option> */}
            {
          countries.map((item,i)=>{
            return (<option key = {i}value = {item.value} disabled = {item.disabled}>{item.label}</option>)
          })}
        </select>

        <select 
        id = "movies"
        value = {movies}
        onChange = {(e)=>{setMovies(e.target.value)}}
        
        >
      
            {
          favMovies.map((item,i)=>{
            return (<option key = {i}value = {item.value} disabled = {item.disabled}>{item.label}</option>)
          })}
        </select>
        <br></br>
        {/* <label >Gender: </label>
        <label htmlFor = "male">male</label>
        <input 
        checked = {gender ==="male"}
        onChange = {(e)=>{setGender(e.target.value)}}
        
        
        type = "radio" id = "male" value = "male"></input>
        <label htmlFor = "female">female</label>
        <input
        checked = {gender ==="female"}
        onChange = {(e)=>{setGender(e.target.value)}}

        
        type = "radio" id = "female" value = "female"></input>
        <label htmlFor = "others">others</label>
        <input 
        checked = {gender ==="others"}
        onChange = {(e)=>{setGender(e.target.value)}}

        type = "radio" id = "others" value = "others"></input>
       */}

<label>Gender: </label>
        {genders.map((item,i)=>{
            return <>
            <label key = {i} htmlFor= {item.value}>{item.label}</label>
            <input checked={gender === item.value}
            onChange={(e)=>{setGender(e.target.value)}}
            type = 'radio' id={item.value} value={item.value}>
            </input>
            </>
            })}




      



     
         
        


        
    
    </form>
  )
}

export default Form2
