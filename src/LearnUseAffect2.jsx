import React, { useEffect, useState } from "react";
const LearnUseEffect2 = () => {
  let [count, setCount] = useState(0);
  useEffect(() => {
    console.log("i am asynchronous useEffect");
    return () => {
      console.log("i am return");
    };
  }, [count]);
  console.log("i am first");
  return (
    <div>
      LearnUseEffect2
      <br></br>
      {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        change count1
      </button>
    </div>
  );
};
export default LearnUseEffect2;