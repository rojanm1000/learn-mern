import React, { useState } from 'react'
import A2 from './Ar2'
import Counter from './Counter'
import HideAndShow from './Hideandshow'
import LearnChildrenProps from './LearnChildrenProps'
import LearnUseEffect2 from './LearnUseAffect2'
import UseState from './LearnUseState'

const Nitan = () => {
  let [showComponent,setShowComponent] = useState(true)
  return (
    <div>
        <UseState></UseState>
        <Counter></Counter>
        <HideAndShow></HideAndShow>
        <A2></A2>
        {/* <LearnUseEffect2></LearnUseEffect2> */}
        <LearnChildrenProps> folk</LearnChildrenProps>
        [showComponent && <LearnUseEffect2></LearnUseEffect2>]
    
    </div>
  )
}

export default Nitan
 