import React, { useReducer } from 'react'

const LearnUseReducer1 = () => {
  let initialValue = 0
  let reducer = (state,action)=>{
    console.log(state)
    console.log("i am reducer")
    console.log(action)
    return 9
  }

  let [state, dispatch] = useReducer(reducer,0)



  return (
    <div>
      {/* LearnUseReducer */}
      <br></br>
      {state}
      <br></br>
      <button onClick = {()=>{
        dispatch("ram")
      }}>Click</button>
      
    </div>
  )
}

export default LearnUseReducer1
