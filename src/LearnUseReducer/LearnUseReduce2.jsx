import React, { useReducer } from 'react'

const LearnUseReduce2 = () => {
    let initialValue = 0
    let reducer = (state,action) =>{
        if (action.type==="increment"){return state+1}
        else if (action.type==="decrement"){return state - 1}
        else if (action.type==="reset"){return initialValue}
    };
    let [count,dispatch] = useReducer(reducer,initialValue)

    
  return (
    <div>
        {count}
        
        <br></br>
        <button onClick = {()=>{
            dispatch({type:"increment"})
        }}>increment</button>
        <br></br>
        <button onClick = {()=>{
            dispatch({type:"decrement"})
        }}>decrement</button>
        <br></br>
        <button onClick = {()=>{
            dispatch({type:"reset"})
        }}>Reset</button>
      
    </div>
  )
}

export default LearnUseReduce2
