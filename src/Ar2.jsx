import React, { useState } from 'react'
import A1 from './Ar1'
const A2 = () => {
    let [show, setShow] = useState(true)
  return (
    <div>
        {show?<p>Show A1</p>: null}
        <button onClick={()=>{setShow(!show)}}><A1></A1></button>
    </div>
  )
}
export default A2